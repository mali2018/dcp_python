import os
import argparse
from skimage.measure import compare_ssim, compare_psnr
import PIL.Image as Image
import cv2


parser = argparse.ArgumentParser()
parser.add_argument('--GT_dir', type=str, default='', help='path to GT')
parser.add_argument('--GR_dir', type=str, default='', help='path to GR')

args = parser.parse_args()

GT_dir = args.GT_dir
GR_dir = args.GR_dir
def PSNRssim(GT_dir, GR_dir):
    """
    :param GT_dir: Ground Truth directory
    :param GR_dir: generated images directory
    :return: average PSNR and SSIM
    """
    GT_img_full_paths = []
    GR_img_full_paths = []
    SSIM = []
    PSNR = []
    temp1 = os.listdir(GT_dir)
    temp1.sort()

    temp2 = os.listdir(GR_dir)
    temp2.sort()
# print(temp1)
    for name in temp1:
        full_path1 = GT_dir + "/" + name
        GT_img_full_paths.append(full_path1)
        print(GT_img_full_paths)
    for name in temp2:
        full_path2 = GR_dir + "/" + name
        GR_img_full_paths.append(full_path2)
        print(GR_img_full_paths)

    for i in range(0, len(GT_img_full_paths)):
        gt = cv2.imread(GT_img_full_paths[i])
        gr = cv2.imread(GR_img_full_paths[i])
        SSIM.append(compare_ssim(gt, gr,multichannel=True))
        PSNR.append(compare_psnr(gt, gr))

    avr_psnr = sum(PSNR)/len(PSNR)
    avr_ssim = sum(SSIM)/len(SSIM)
    return avr_psnr, avr_ssim

avr_psnr, avr_ssim = PSNRssim(GT_dir, GR_dir)
print('avr_psnr:{0}, avr_ssim_ssim:{1}'.format(avr_psnr, avr_ssim))
